import hashlib
""" hash: metodologia con la que se encripta la información, de tal forma que una máquina externa no pueda interpretar la información
    En python 3 se debe codificar en utf-8 u otro, antes de pasar al hash
"""

def main():
    clave = str(input("Ingresa la palabra a transformar: ")).encode('utf-8')
    md5 = hashlib.md5(clave).hexdigest()
    print("Este es un hash MD5: "+ md5)

    sha1 = hashlib.sha1(clave).hexdigest()
    print("Este es un hash SHA1: "+ sha1)

    sha224 = hashlib.sha224(clave).hexdigest()
    print("Este es un hash SHA224: "+ sha224)

    sha384 = hashlib.sha384(clave).hexdigest()
    print("Este es un hash SHA384: "+ sha384)

    sha512 = hashlib.sha512(clave).hexdigest()
    print("Este es un hash SHA512: "+ sha512)

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        exit()
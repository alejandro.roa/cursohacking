import hashlib

def main():
    resolverHash = str(input("Ingrese el hash a resolver: "))
    resolvedor = open("resolvedordeclaves.txt","r")

    for x in resolvedor.readlines():
        a = x.strip("\n").encode("utf-8")
        a = hashlib.sha1(a).hexdigest()
        if a == resolverHash:
            print("Clave: ",x,"El hash resuelto: ",a,sep=" ")

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        exit()